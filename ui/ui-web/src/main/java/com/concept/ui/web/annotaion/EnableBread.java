package com.concept.ui.web.annotaion;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableBread {
    String path() default "";

    BreadMethod[] methods() default {};
}