package com.concept.ui.web.metadata;

public enum Bread {
    ADD,
    EDIT,
    DELETE,
    READ,
    BROWSE
}
