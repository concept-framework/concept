package com.concept.ui.web.message;

import com.concept.core.message.MessageHandler;

public enum Messages implements MessageHandler {
    ENTITY_NOT_FOUND,
    ID_IS_REQUIRED,
    JSON_BODY_IS_INVALID,
    NO_MESSAGE_AVAILABLE;
}
