package com.concept.oauth.core.payload;

public class MfaStatusRequest {
    private boolean isActive;

    private boolean sent;

    private boolean isPassed;

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean getSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public boolean isPassed() {
        return !isActive || isPassed;
    }

    public boolean isNonePassed() {
        return !isPassed();
    }

    public void setPassed(boolean passed) {
        isPassed = passed;
    }

    public MfaStatusRequest() {
    }

    public MfaStatusRequest(boolean isActive) {
        this(isActive, !isActive);
    }

    public MfaStatusRequest(boolean isActive, boolean isPass) {
        this.isActive = isActive;
        this.isPassed = isPass;
    }

    @Override
    public String toString() {
        return "TwoFactorAuthenticationInfoRequest{" +
                "isActive=" + isActive +
                ", isPassed=" + isPassed +
                '}';
    }
}
