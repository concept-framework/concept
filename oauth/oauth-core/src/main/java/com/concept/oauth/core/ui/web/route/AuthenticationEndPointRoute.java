package com.concept.oauth.core.ui.web.route;

import org.springframework.http.HttpMethod;

public class AuthenticationEndPointRoute {
    public final static String PATH = "/oauth";

    public static class SignUp {
        public final static String PATH = AuthenticationEndPointRoute.PATH + "/signup";
        public final static HttpMethod METHOD = HttpMethod.POST;
    }

    public static class SignInPage {
        public final static String PATH = AuthenticationEndPointRoute.PATH + "/signin";
        public final static HttpMethod METHOD = HttpMethod.GET;
    }

    public static class VerificationPage {
        public final static String PATH = AuthenticationEndPointRoute.PATH + "/verification";
        public final static HttpMethod METHOD = HttpMethod.GET;
    }

    public static class Verification {
        public final static String PATH = AuthenticationEndPointRoute.PATH + "/verification";
        public final static HttpMethod METHOD = HttpMethod.POST;
    }

    public static class SignOut {
        public final static String PATH = AuthenticationEndPointRoute.PATH + "/signout";
        public final static HttpMethod METHOD = HttpMethod.GET;
    }

    public static class Reset {
        public final static String PATH = AuthenticationEndPointRoute.PATH;
        public final static HttpMethod METHOD = HttpMethod.DELETE;
    }
}
