package com.concept.oauth.core.metadata;

import org.apache.commons.lang3.StringUtils;

public interface Scope {

    String name();

    static Scope[] values() {
        return new Scope[0];
    }

    static Scope nameOf(String s) {
        for (Scope value : values())
            if (StringUtils.equalsIgnoreCase(s, value.name())) return value;
        return null;
    }
}
