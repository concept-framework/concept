package com.concept.oauth.core.metadata;

public enum PasswordKind {
    PASSWORD,
    S2FA,
    E2FA,
    G2FA
}
