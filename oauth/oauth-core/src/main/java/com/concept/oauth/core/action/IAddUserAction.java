package com.concept.oauth.core.action;

import com.concept.context.action.intefaces.IAction2;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.oauth.core.model.interfaces.IUserDetail;

public interface IAddUserAction <TEntity extends IUserDetail, TClientDetail extends IClientDetail, Object> extends IAction2<TEntity, TClientDetail, Object> {
}
