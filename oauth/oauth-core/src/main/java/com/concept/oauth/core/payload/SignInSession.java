package com.concept.oauth.core.payload;

import com.concept.oauth.core.metadata.PasswordKind;

public class SignInSession {
    private String clientId;

    private PasswordKind passwordKind;

    private MfaStatusSession mfa;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public PasswordKind getPasswordKind() {
        return passwordKind;
    }

    public void setPasswordKind(PasswordKind passwordKind) {
        this.passwordKind = passwordKind;
    }

    public MfaStatusSession getMfa() {
        return mfa == null ? new MfaStatusSession() : mfa;
    }

    public void setMfa(MfaStatusSession mfa) {
        this.mfa = mfa;
    }

    @Override
    public String toString() {
        return "LoginSession{" +
                "clientId='" + clientId + '\'' +
                ", passwordKind=" + passwordKind +
                ", mfa=" + mfa +
                '}';
    }
}
