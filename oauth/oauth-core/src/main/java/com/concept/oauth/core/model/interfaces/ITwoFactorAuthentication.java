package com.concept.oauth.core.model.interfaces;

import com.concept.data.core.model.ICoreEmbeddable;

public interface ITwoFactorAuthentication extends ICoreEmbeddable {
    String getSecretKey();

    boolean isActiveG2fa();

    boolean isActiveS2fa();

    boolean isActiveE2fa();
}
