package com.concept.oauth.core.exception;

import com.concept.core.exception.HttpExceptionHandling;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Order(value = 99)
public class SecurityExceptionHandling extends HttpExceptionHandling {

    @ExceptionHandler({ClientNotFoundException.class})
    public ResponseEntity handleUpgradeException(ClientNotFoundException ex, HttpServletRequest req) {
        return handleException(ex, HttpStatus.NOT_FOUND, req);
    }

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity handleUpgradeException(UserNotFoundException ex, HttpServletRequest req) {
        return handleException(ex, HttpStatus.NOT_FOUND, req);
    }

    @ExceptionHandler({AuthenticationException.class})
    public ResponseEntity handleUpgradeException(AuthenticationException ex, HttpServletRequest req) {
        return handleException(ex, HttpStatus.UNAUTHORIZED, req);
    }
}
