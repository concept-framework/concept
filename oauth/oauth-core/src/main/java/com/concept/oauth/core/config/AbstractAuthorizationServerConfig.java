package com.concept.oauth.core.config;

import com.concept.oauth.core.OAuthConfig;
import com.concept.oauth.core.action.IClientLoaderAction;
import com.concept.oauth.core.action.IUserLoaderAction;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.approval.ApprovalStore;
import org.springframework.security.oauth2.provider.approval.ApprovalStoreUserApprovalHandler;
import org.springframework.security.oauth2.provider.approval.TokenApprovalStore;
import org.springframework.security.oauth2.provider.approval.UserApprovalHandler;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class AbstractAuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {
        configurer.jdbc(getDataSource());
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenStore(getTokenStore())
                .requestFactory(getOAuth2RequestFactory())
                .accessTokenConverter(getAccessTokenConverter())
                .authenticationManager(getAuthenticationManager())
                .userDetailsService(getUserLoaderAction())
                .tokenGranter(getTokenGranter())
                .authorizationCodeServices(getAuthorizationCodeServices())
                .userApprovalHandler(getUserApprovalHandler());
    }

    protected abstract DataSource getDataSource();

    protected abstract TokenStore getTokenStore();

    protected abstract <TClientLoader extends IClientLoaderAction<? extends IClientDetail>> TClientLoader getClientLoaderAction();

    protected abstract <TUserLoader extends IUserLoaderAction<? extends IUserDetail>> TUserLoader getUserLoaderAction();

    protected abstract JwtAccessTokenConverter getAccessTokenConverter();

    protected abstract AuthenticationManager getAuthenticationManager();

    protected abstract BCryptPasswordEncoder getPasswordEncoder();

    protected abstract TwoFactorAuthenticationFilter getTwoFactorAuthenticationFilter();

    protected List<TokenGranter> getTokenGranters() {
        List<TokenGranter> tokenGranters = new ArrayList<>();
        tokenGranters.add(new AuthorizationCodeTokenGranter(getAuthorizationServerTokenServices(), getAuthorizationCodeServices(), getClientLoaderAction(), getOAuth2RequestFactory()));
        tokenGranters.add(new RefreshTokenGranter(getAuthorizationServerTokenServices(), getClientLoaderAction(), getOAuth2RequestFactory()));
        tokenGranters.add(new ClientCredentialsTokenGranter(getAuthorizationServerTokenServices(), getClientLoaderAction(), getOAuth2RequestFactory()));
        tokenGranters.add(new ImplicitTokenGranter(getAuthorizationServerTokenServices(), getClientLoaderAction(), getOAuth2RequestFactory()));
        if (getAuthenticationManager() != null) {
            tokenGranters.add(new ResourceOwnerPasswordTokenGranter(getAuthenticationManager(), getAuthorizationServerTokenServices(), getClientLoaderAction(), getOAuth2RequestFactory()));
        }
        return tokenGranters;
    }

    protected AuthorizationCodeServices getAuthorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(getDataSource());
    }

    protected DefaultOAuth2RequestFactory getOAuth2RequestFactory() {
        return new OAuth2RequestFactory(getClientLoaderAction());
    }

    protected TokenGranter getTokenGranter() {
        return new TokenGranter() {
            private CompositeTokenGranter compositeTokenGranter;
            @Override
            public OAuth2AccessToken grant(String grantType, TokenRequest tokenRequest) {
                if (compositeTokenGranter == null) {
                    compositeTokenGranter = new CompositeTokenGranter(getTokenGranters());
                }
                return compositeTokenGranter.grant(grantType, tokenRequest);
            }
        };
    }

    protected UserApprovalHandler getUserApprovalHandler() {
        ApprovalStoreUserApprovalHandler userApprovalHandler = new ApprovalStoreUserApprovalHandler();
        userApprovalHandler.setApprovalStore(getApprovalStore());
        userApprovalHandler.setClientDetailsService(getClientLoaderAction());
        userApprovalHandler.setRequestFactory(getOAuth2RequestFactory());
        return userApprovalHandler;
    }


    protected ApprovalStore getApprovalStore() {
        TokenApprovalStore approvalStore = new TokenApprovalStore();
        approvalStore.setTokenStore(getTokenStore());
        return approvalStore;
    }

    protected DefaultTokenServices getAuthorizationServerTokenServices() {
        DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(getTokenStore());
        tokenServices.setReuseRefreshToken(false);
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setClientDetailsService(getClientLoaderAction());
        tokenServices.setTokenEnhancer(getAccessTokenConverter());
        this.setPreAuthenticatedUserDetailsService(tokenServices, getUserLoaderAction());
        return tokenServices;
    }

    private void setPreAuthenticatedUserDetailsService(DefaultTokenServices tokenServices, UserDetailsService userDetailsService) {
        if (userDetailsService != null) {
            PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
            provider.setPreAuthenticatedUserDetailsService(new UserDetailsByNameServiceWrapper(userDetailsService));
            tokenServices.setAuthenticationManager(new ProviderManager(Collections.singletonList(provider)));
        }
    }

    @Bean
    public FilterRegistrationBean twoFactorAuthenticationFilterRegistration(){
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(getTwoFactorAuthenticationFilter());
        registration.addUrlPatterns("/oauth/authorize");
        registration.setName("twoFactorAuthenticationFilter");
        return registration;
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(OAuthConfig.SECURITY_SIGN_KEY);
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(getTokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Bean
    public UserAuthenticationProvider daoAuthenticationProvider() {
        UserAuthenticationProvider provider = new UserAuthenticationProvider();
        provider.setPasswordEncoder(getPasswordEncoder());
        provider.setUserDetailsService(getUserLoaderAction());
        return provider;
    }
}
