package com.concept.oauth.core.message;

import com.concept.core.message.MessageHandler;

public enum Messages implements MessageHandler {
    CLIENT_ID_NOT_FOUND,
    USERNAME_OR_PASSWORD_IS_INVALID,
    PAGE_NOT_FOUND,
    SMS_VERIFICATION_TITLE,
    EMAIL_VERIFICATION_TITLE,
    GOOGLE_AUTHENTICATOR_VERIFICATION_TITLE,
    VERIFICATION_CODE_IS_INVALID,

}
