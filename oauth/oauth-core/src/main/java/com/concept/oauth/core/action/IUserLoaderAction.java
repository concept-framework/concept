package com.concept.oauth.core.action;

import com.concept.oauth.core.exception.UserNotFoundException;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.context.action.intefaces.IAction1;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface IUserLoaderAction<TEntity extends IUserDetail> extends IAction1<TEntity, String>, UserDetailsService {

    default TEntity loadUserByUsername(String var1) throws UsernameNotFoundException {
        try {
            return execute(var1);
        } catch (Throwable throwable) {
            throw new UserNotFoundException(throwable.getMessage());
        }
    }
}
