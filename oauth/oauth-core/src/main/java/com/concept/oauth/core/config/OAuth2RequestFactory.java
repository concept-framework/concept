package com.concept.oauth.core.config;

import com.concept.oauth.core.OAuthConfig;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.Map;


public class OAuth2RequestFactory extends DefaultOAuth2RequestFactory {

    OAuth2RequestFactory(ClientDetailsService clientDetailsService) {
        super(clientDetailsService);
    }

    @Override
    public AuthorizationRequest createAuthorizationRequest(Map<String, String> authorizationParameters) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(false);
        if (session != null) {
            AuthorizationRequest authorizationRequest = (AuthorizationRequest) session.getAttribute(OAuthConfig.AUTHORIZATION_REQUEST_SESSION_KEY);
            if (authorizationRequest != null) {
                return authorizationRequest;
            }
        }
        return super.createAuthorizationRequest(authorizationParameters);
    }
}
