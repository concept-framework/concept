package com.concept.oauth.core.metadata;

import org.apache.commons.lang3.StringUtils;

public enum MfaKind {
    S2FA,
    E2FA,
    G2FA;

    static MfaKind nameOf(String s) {
        for (MfaKind value : values())
            if (StringUtils.equalsIgnoreCase(s, value.name())) return value;
        return null;
    }
}
