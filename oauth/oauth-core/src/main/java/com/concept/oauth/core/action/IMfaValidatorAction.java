package com.concept.oauth.core.action;

import com.concept.oauth.core.metadata.MfaKind;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.context.action.intefaces.IAction3;

public interface IMfaValidatorAction<TEntity extends IUserDetail> extends IAction3<Boolean, MfaKind, TEntity, String> {

}
