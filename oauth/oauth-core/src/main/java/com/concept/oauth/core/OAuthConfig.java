package com.concept.oauth.core;

import org.apache.commons.lang3.StringUtils;

public class OAuthConfig {
    public static final String AUTHORIZATION_REQUEST_SESSION_KEY = "SAVED_AUTHORIZATION_REQUEST";
    public static final String OAUTH_SIGN_IN_SESSION_KEY = "OAUTH_SIGN_IN";
    public static final String SECURITY_SIGN_KEY = "MaYzkSjmkzPC57L";
    public static String DEFAULT_CLIENT_ID = StringUtils.EMPTY;
}
