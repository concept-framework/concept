package com.concept.oauth.core.model.interfaces;

import com.concept.data.core.model.ICoreEntity;
import com.concept.oauth.core.metadata.PasswordKind;
import org.springframework.security.oauth2.provider.ClientDetails;

public interface IClientDetail extends ICoreEntity, ClientDetails {

    PasswordKind getPasswordKind();

}
