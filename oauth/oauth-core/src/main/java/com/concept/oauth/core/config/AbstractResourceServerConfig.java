package com.concept.oauth.core.config;

import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

public abstract class AbstractResourceServerConfig extends ResourceServerConfigurerAdapter {

    protected abstract String getResourceId();

    protected abstract ResourceServerTokenServices getResourceServerTokenServices();

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.resourceId(getResourceId()).tokenServices(getResourceServerTokenServices());
    }

}
