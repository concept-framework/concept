package com.concept.oauth.core.config;

import com.concept.core.log.Logger;
import com.concept.oauth.core.OAuthConfig;
import com.concept.oauth.core.action.IClientLoaderAction;
import com.concept.oauth.core.exception.AuthenticationException;
import com.concept.oauth.core.metadata.PasswordKind;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.oauth.core.message.Messages;
import com.concept.oauth.core.payload.SignInSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

public class UserAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired
    private IClientLoaderAction clientLoaderAction;

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        IClientDetail clientDetail = clientLoaderAction.loadClientByClientId(getClientId());
        if (clientDetail != null && !PasswordKind.PASSWORD.equals(clientDetail.getPasswordKind()))
            return;
        if (authentication.getCredentials() == null) {
            Logger.debug("Authentication failed", "no credentials provided");
            throw new AuthenticationException(Messages.USERNAME_OR_PASSWORD_IS_INVALID);
        } else {
            String presentedPassword = authentication.getCredentials().toString();
            if (!this.getPasswordEncoder().matches(presentedPassword, userDetails.getPassword())) {
                Logger.debug("Authentication failed", "password does not match stored value");
                throw new AuthenticationException(Messages.USERNAME_OR_PASSWORD_IS_INVALID);
            }
        }
    }

    private String getClientId() {
        SignInSession signInSession = (SignInSession) getSession().getAttribute(OAuthConfig.OAUTH_SIGN_IN_SESSION_KEY);
        return signInSession == null || StringUtils.isBlank(signInSession.getClientId()) ? StringUtils.EMPTY : signInSession.getClientId();
    }

    private HttpSession getSession() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession();
    }
}
