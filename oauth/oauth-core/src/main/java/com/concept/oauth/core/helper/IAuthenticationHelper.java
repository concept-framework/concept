package com.concept.oauth.core.helper;

import com.concept.core.application.ApplicationContextProvider;
import com.concept.oauth.core.OAuthConfig;
import com.concept.oauth.core.action.IUserLoaderAction;
import com.concept.oauth.core.metadata.Scope;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.oauth.core.payload.SignInSession;
import com.concept.oauth.core.payload.MfaStatusSession;
import com.concept.ui.web.helper.IWebHelper;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public interface IAuthenticationHelper<TEntity extends IUserDetail> extends IWebHelper {

    default boolean isAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

    default void clearAuthenticate() {
        SecurityContextHolder.clearContext();
    }

    default TEntity getCurrentUser() {
        IUserLoaderAction<TEntity> loader = ApplicationContextProvider.context.getBean(IUserLoaderAction.class);
        TEntity user = isAuthenticated() ? (TEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal() : null;
        return user == null ? null : loader.loadUserByUsername(user.getUsername());
    }

    default Set<Scope> getScopes() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        OAuth2Authentication oAuth2Authentication;
        if (authentication instanceof OAuth2Authentication) {
            oAuth2Authentication = (OAuth2Authentication) authentication;
        } else {
            return new HashSet<>();
        }
        return oAuth2Authentication.getOAuth2Request().getScope() == null || oAuth2Authentication.getOAuth2Request().getScope().isEmpty() ? new HashSet<>() : oAuth2Authentication.getOAuth2Request().getScope().stream().map(Scope::nameOf).collect(Collectors.toSet());
    }

    default Scope getScope() {
        return getScopes() == null || getScopes().isEmpty() ? null : getScopes().stream().findFirst().orElse(null);
    }

    default void addRole(Collection<? extends GrantedAuthority> authorities) {
        if (authorities == null || authorities.isEmpty())
            return;
        for (GrantedAuthority authority : authorities) {
            addRole(authority);
        }
    }

    default void addRole(GrantedAuthority authority) {
        Collection<SimpleGrantedAuthority> oldAuthorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        Set<GrantedAuthority> updatedAuthorities = new HashSet<>();
        updatedAuthorities.add(authority);
        updatedAuthorities.addAll(oldAuthorities);
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
                        SecurityContextHolder.getContext().getAuthentication().getCredentials(),
                        updatedAuthorities));
    }

    default void autoLogin(TEntity user) {
        if (user == null)
            return;
        Collection<SimpleGrantedAuthority> oldAuthorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        Set<GrantedAuthority> updatedAuthorities = new HashSet<>();
        updatedAuthorities.addAll(user.getAuthorities());
        updatedAuthorities.addAll(oldAuthorities);
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        user,
                        null,
                        updatedAuthorities));
    }

    default SignInSession getSignInSession() {
        SignInSession signInSession = (SignInSession) getSession().getAttribute(OAuthConfig.OAUTH_SIGN_IN_SESSION_KEY);
        return signInSession == null ? new SignInSession() : signInSession;
    }

    default String getClientId() {
        return getSignInSession().getClientId();
    }

    default MfaStatusSession getMfaStatusSession() {
        MfaStatusSession mfaStatusSession = getSignInSession().getMfa() != null ? getSignInSession().getMfa() : isAuthenticated() ? new MfaStatusSession(getCurrentUser().getTwoFactorAuthentication()) : new MfaStatusSession();
        getSignInSession().setMfa(mfaStatusSession);
        return mfaStatusSession;
    }

    default boolean isPassS2fa() {
        return getMfaStatusSession().getS2fa() == null || getMfaStatusSession().getS2fa().isPassed();
    }

    default boolean isNonePassS2fa() {
        return !isPassS2fa();
    }

    default boolean isPassE2fa() {
        return getMfaStatusSession().getE2fa() == null || getMfaStatusSession().getE2fa().isPassed();
    }

    default boolean isNonePassE2fa() {
        return !isPassE2fa();
    }

    default boolean isPassG2fa() {
        return getMfaStatusSession().getG2fa() == null || getMfaStatusSession().getG2fa().isPassed();
    }

    default boolean isNonePassG2fa() {
        return !isPassG2fa();
    }

    default boolean isPassMfa() {
        return getMfaStatusSession().isPass();
    }

    default boolean isNonePassMfa() {
        return !isPassMfa();
    }

}
