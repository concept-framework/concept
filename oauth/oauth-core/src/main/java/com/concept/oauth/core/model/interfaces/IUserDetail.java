package com.concept.oauth.core.model.interfaces;

import com.concept.data.core.model.ICoreEntity;
import org.springframework.security.core.userdetails.UserDetails;

public interface IUserDetail extends ICoreEntity, UserDetails {

    ITwoFactorAuthentication getTwoFactorAuthentication();

}
