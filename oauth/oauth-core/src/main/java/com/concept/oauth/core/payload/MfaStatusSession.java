package com.concept.oauth.core.payload;


import com.concept.oauth.core.model.interfaces.ITwoFactorAuthentication;

public class MfaStatusSession {
    private MfaStatusRequest s2fa = new MfaStatusRequest(false);

    private MfaStatusRequest e2fa = new MfaStatusRequest(false);

    private MfaStatusRequest g2fa = new MfaStatusRequest(false);

    public MfaStatusRequest getS2fa() {
        return s2fa;
    }

    public void setS2fa(MfaStatusRequest s2fa) {
        this.s2fa = s2fa;
    }

    public MfaStatusRequest getE2fa() {
        return e2fa;
    }

    public void setE2fa(MfaStatusRequest e2fa) {
        this.e2fa = e2fa;
    }

    public MfaStatusRequest getG2fa() {
        return g2fa;
    }

    public void setG2fa(MfaStatusRequest g2fa) {
        this.g2fa = g2fa;
    }

    public boolean isPass() {
        boolean s2faIsPass = getS2fa() == null || getS2fa().isPassed();
        boolean e2faIsPass = getE2fa() == null || getE2fa().isPassed();
        boolean g2faIsPass = getG2fa() == null || getG2fa().isPassed();
        return s2faIsPass && e2faIsPass && g2faIsPass;
    }

    public boolean isNonePassed() {
        return !isPass();
    }

    public MfaStatusSession() {
    }

    public MfaStatusSession(ITwoFactorAuthentication twoFactorAuthentication) {
        if (twoFactorAuthentication != null) {
            this.s2fa = new MfaStatusRequest(twoFactorAuthentication.isActiveS2fa(), !twoFactorAuthentication.isActiveE2fa());
            this.e2fa = new MfaStatusRequest(twoFactorAuthentication.isActiveE2fa(), !twoFactorAuthentication.isActiveE2fa());
            this.g2fa = new MfaStatusRequest(twoFactorAuthentication.isActiveG2fa(), !twoFactorAuthentication.isActiveG2fa());
        }
    }

    public MfaStatusSession(MfaStatusRequest s2fa, MfaStatusRequest e2fa, MfaStatusRequest g2fa) {
        this.s2fa = s2fa;
        this.e2fa = e2fa;
        this.g2fa = g2fa;
    }

    @Override
    public String toString() {
        return "M2faStatusSession{" +
                "s2fa=" + s2fa +
                ", e2fa=" + e2fa +
                ", g2fa=" + g2fa +
                '}';
    }
}
