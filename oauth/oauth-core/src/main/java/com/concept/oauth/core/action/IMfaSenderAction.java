package com.concept.oauth.core.action;

import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.oauth.core.metadata.MfaKind;
import com.concept.context.action.intefaces.IAction2;

public interface IMfaSenderAction<TEntity extends IUserDetail> extends IAction2<Boolean, MfaKind, TEntity> {

}
