package com.concept.oauth.core.ui.web.api;

import com.concept.core.exception.BadRequestException;
import com.concept.core.log.Logger;
import com.concept.oauth.core.action.IAddUserAction;
import com.concept.oauth.core.helper.IAuthenticationHelper;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.oauth.core.payload.MfaStatusSession;
import com.concept.oauth.core.ui.web.route.AuthenticationEndPointRoute;
import com.concept.oauth.core.action.IClientLoaderAction;
import com.concept.oauth.core.action.IMfaSenderAction;
import com.concept.oauth.core.action.IMfaValidatorAction;
import com.concept.oauth.core.message.Messages;
import com.concept.oauth.core.metadata.MfaKind;
import com.concept.oauth.core.metadata.PasswordKind;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.ui.web.controller.BaseController;
import com.concept.ui.web.helper.IWebHelper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.endpoint.FrameworkEndpoint;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@FrameworkEndpoint
public class AuthenticationEndPoint extends BaseController implements IWebHelper, IAuthenticationHelper<IUserDetail> {

    private final IClientLoaderAction clientLoaderAction;

    private final IMfaValidatorAction mfaValidatorAction;

    private final IMfaSenderAction mfaSenderAction;

    private final IAddUserAction addUserAction;

    public AuthenticationEndPoint(IClientLoaderAction clientLoaderAction, IMfaSenderAction mfaSenderAction, IMfaValidatorAction mfaValidatorAction, IAddUserAction addUserAction) {
        this.clientLoaderAction = clientLoaderAction;
        this.mfaSenderAction = mfaSenderAction;
        this.mfaValidatorAction = mfaValidatorAction;
        this.addUserAction = addUserAction;
    }

    @RequestMapping(method = RequestMethod.POST, value = AuthenticationEndPointRoute.SignUp.PATH, produces = MediaType.APPLICATION_JSON_VALUE)
    public String signUp(@RequestBody HashMap<String, Object> request, Model model) throws Throwable {
        if (StringUtils.isBlank(getClientId())) {
            model.addAttribute("errorMessage", Messages.PAGE_NOT_FOUND.getMessage());
            return "message";
        }
        IClientDetail clientDetail = clientLoaderAction.loadClientByClientId(getClientId());
        if (request.containsKey("password")) {
            String password = request.get("password") == null || StringUtils.isBlank(request.get("password").toString())? StringUtils.EMPTY : new BCryptPasswordEncoder().encode(request.get("password").toString());
            request.put("password", password);
        } else if (request.containsKey("pass")) {
            String password = request.get("pass") == null || StringUtils.isBlank(request.get("pass").toString())? StringUtils.EMPTY : new BCryptPasswordEncoder().encode(request.get("pass").toString());
            request.put("pass", password);
        }
        IUserDetail user = (IUserDetail) addUserAction.execute(clientDetail, request);
        getSignInSession().setMfa(new MfaStatusSession(user.getTwoFactorAuthentication()));
        return verificationPage(model);
    }

    @RequestMapping(method = RequestMethod.GET, value = AuthenticationEndPointRoute.SignInPage.PATH)
    public String signIn(Model model) {
        if (StringUtils.isBlank(getClientId())) {
            model.addAttribute("errorMessage", Messages.PAGE_NOT_FOUND.getMessage());
            return "message";
        }
        if (isAuthenticated())
            return isPassMfa() ? successLogin() : verificationPage(model);
        IClientDetail clientDetail = clientLoaderAction.loadClientByClientId(getClientId());
        PasswordKind passwordKind = clientDetail == null || clientDetail.getPasswordKind() == null ? PasswordKind.PASSWORD : clientDetail.getPasswordKind();
        model.addAttribute("passwordKind", passwordKind.name());
        return "signin";
    }

    @RequestMapping(method = RequestMethod.GET, value = AuthenticationEndPointRoute.VerificationPage.PATH)
    public String verificationPage(Model model) {
        if (!isAuthenticated())
            return signIn(model);
        if (isPassMfa())
            return successLogin();
        model.addAttribute("kind", getMfaKind().name());
        model.addAttribute("username", getCurrentUser().getUsername());
        return "verification";
    }

    @RequestMapping(method = RequestMethod.POST, value = AuthenticationEndPointRoute.Verification.PATH)
    public String verification(@ModelAttribute(value = "code") String code, Model model) {
        if (!isAuthenticated())
            return verificationPage(model);
        boolean isValidCode = false;
        if (isPassMfa())
            return successLogin();
        if (isNonePassS2fa() && isValidMfa(MfaKind.S2FA, getCurrentUser(), code)) {
            getMfaStatusSession().getS2fa().setPassed(true);
            isValidCode = true;
        } else if (isNonePassE2fa() && isValidMfa(MfaKind.E2FA, getCurrentUser(), code)) {
            getMfaStatusSession().getE2fa().setPassed(true);
            isValidCode = true;
        } else if (isNonePassG2fa() && isValidMfa(MfaKind.G2FA, getCurrentUser(), code)) {
            getMfaStatusSession().getG2fa().setPassed(true);
            isValidCode = true;
        }
        if (isPassMfa())
            return successLogin();
        if (!isValidCode) {
            model.addAttribute("username", getCurrentUser().getUsername());
            model.addAttribute("isError", true);
            model.addAttribute("errorMessage", Messages.VERIFICATION_CODE_IS_INVALID.getMessage());
        }
        model.addAttribute("kind", getMfaKind().name());
        return "verification";
    }

    @RequestMapping(method = RequestMethod.GET, value = AuthenticationEndPointRoute.SignOut.PATH)
    public void signOut(@ModelAttribute(value = "client_id") String client_id, HttpServletRequest request, HttpServletResponse response) {
        try {
            client_id = StringUtils.isBlank(client_id) ? getClientId() : client_id;
            if (StringUtils.isBlank(client_id))
                throw new BadRequestException(Messages.CLIENT_ID_NOT_FOUND);
            SecurityContextHolder.clearContext();
            SecurityContextHolder.getContext().setAuthentication(null);
            if (getSession() != null) {
                getSession().invalidate();
            }
            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    cookie.setMaxAge(0);
                }
            }
            request.getSession(true);
            response.sendRedirect(request.getContextPath() + "/oauth/authorize?response_type=code&client_id=" + client_id);
        } catch (IOException | BadRequestException e) {
            Logger.error("signout", e.getMessage());
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = AuthenticationEndPointRoute.Reset.PATH)
    public void reset(HttpServletRequest request, HttpServletResponse response) throws IOException {
        clearAuthenticate();
        response.sendRedirect(request.getContextPath() + "/oauth/authorize?response_type=code&client_id=" + getClientId());
    }

    private MfaKind getMfaKind() {
        if (getMfaStatusSession().getS2fa().isNonePassed()) {
            if (getMfaStatusSession().getS2fa().getSent()) {
                boolean sent = sendMfaVerificationCode(MfaKind.S2FA, getCurrentUser());
                getMfaStatusSession().getS2fa().setSent(sent);
            }
            return MfaKind.S2FA;
        }
        if (getMfaStatusSession().getE2fa().isNonePassed()) {
            if (getMfaStatusSession().getE2fa().getSent()) {
                boolean sent = sendMfaVerificationCode(MfaKind.E2FA, getCurrentUser());
                getMfaStatusSession().getE2fa().setSent(sent);
            }
            return MfaKind.E2FA;
        }
        if (getMfaStatusSession().getG2fa().isNonePassed()) {
            if (getMfaStatusSession().getG2fa().getSent()) {
                boolean sent = sendMfaVerificationCode(MfaKind.G2FA, getCurrentUser());
                getMfaStatusSession().getG2fa().setSent(sent);
            }
            return MfaKind.G2FA;
        }
        return MfaKind.S2FA;
    }

    private boolean sendMfaVerificationCode(MfaKind kind, IUserDetail user) {
        try {
            return (boolean) mfaSenderAction.execute(kind, user);
        } catch (Throwable e) {
            Logger.error("Validation" + kind, e.getMessage());
            return false;
        }
    }

    private boolean isValidMfa(MfaKind kind, IUserDetail user, String code) {
        try {
            return (boolean) mfaValidatorAction.execute(kind, user, code);
        } catch (Throwable e) {
            Logger.error("Validation" + kind, e.getMessage());
            return false;
        }
    }

    private String successLogin() {
        IUserDetail user = getCurrentUser();
        if (user != null && user.getAuthorities() != null && !user.getAuthorities().isEmpty()) {
            addRole(user.getAuthorities());
        }
        return "forward:/oauth/authorize";
    }

}
