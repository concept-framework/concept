package com.concept.oauth.core.exception;

import com.concept.core.exception.HttpErrorMessage;
import com.concept.core.exception.IHttpException;
import com.concept.core.message.MessageHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.util.Map;

public class AuthenticationException extends org.springframework.security.core.AuthenticationException  implements IHttpException {
    private final HttpErrorMessage errorMessage;

    public AuthenticationException(MessageHandler message) {
        super(message.getMessage());
        errorMessage = new HttpErrorMessage(getHttpStatus(), message.getMessage(), message.getCode());
    }

    public AuthenticationException(String message) {
        super(message);
        errorMessage = new HttpErrorMessage(getHttpStatus(), message, null);
    }

    public AuthenticationException(HttpErrorMessage errorMessage) {
        super(errorMessage.getMessage());
        this.errorMessage = errorMessage;
    }

    public void setPath(String path) {
        if (errorMessage != null)
            errorMessage.setPath(path);
    }

    public HttpErrorMessage getErrorMessage() {
        return errorMessage;
    }

    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    public Map<String, Serializable> body() {
        return errorMessage.body();
    }

    @Override
    public String toString() {
        return toString(errorMessage);
    }

    protected String toString(Object value) {
        try {
            ObjectMapper Obj = new ObjectMapper();
            return Obj.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            return "";
        }
    }
}
