package com.concept.oauth.core.config;

import com.concept.core.log.Logger;
import com.concept.oauth.core.OAuthConfig;
import com.concept.oauth.core.action.IClientLoaderAction;
import com.concept.oauth.core.helper.IAuthenticationHelper;
import com.concept.oauth.core.metadata.PasswordKind;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.oauth.core.model.interfaces.IUserDetail;
import com.concept.oauth.core.ui.web.route.AuthenticationEndPointRoute;
import com.concept.oauth.core.payload.SignInSession;
import com.concept.oauth.core.payload.MfaStatusRequest;
import com.concept.oauth.core.payload.MfaStatusSession;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.util.OAuth2Utils;
import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@Component
public class TwoFactorAuthenticationFilter extends OncePerRequestFilter implements IAuthenticationHelper {
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    private OAuth2RequestFactory oAuth2RequestFactory;

    private IClientLoaderAction clientLoaderAction;

    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;

    @Autowired
    public void setClientDetailsService(IClientLoaderAction clientLoaderAction) {
        this.clientLoaderAction = clientLoaderAction;
        oAuth2RequestFactory = new OAuth2RequestFactory(clientLoaderAction);
    }

    private Map<String, String> paramsFromRequest(HttpServletRequest request) {
        Map<String, String> params = new HashMap<>();
        for (Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            params.put(entry.getKey(), entry.getValue()[0]);
        }
        return params;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            Map<String, String> params = paramsFromRequest(request);
            SignInSession signInSession = (SignInSession) request.getSession().getAttribute(OAuthConfig.OAUTH_SIGN_IN_SESSION_KEY);
            signInSession = signInSession == null ? new SignInSession() : signInSession;
            String clientId = StringUtils.isNoneBlank(signInSession.getClientId()) ? signInSession.getClientId() : params.get(OAuth2Utils.CLIENT_ID);
            IClientDetail clientDetail = clientLoaderAction.loadClientByClientId(clientId);
            PasswordKind passwordKind = clientDetail == null || clientDetail.getPasswordKind() == null ? PasswordKind.PASSWORD : clientDetail.getPasswordKind();
            signInSession.setClientId(clientId);
            signInSession.setPasswordKind(passwordKind);
            if (isAuthenticated()) {
                IUserDetail user = (IUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                signInSession.setMfa(signInSession.getMfa() == null ? new MfaStatusSession(user.getTwoFactorAuthentication()) : signInSession.getMfa());
                if (PasswordKind.S2FA.equals(passwordKind)) {
                    signInSession.getMfa().setS2fa(new MfaStatusRequest(true));
                } else if (PasswordKind.E2FA.equals(passwordKind)) {
                    signInSession.getMfa().setE2fa(new MfaStatusRequest(true));
                } else if (PasswordKind.G2FA.equals(passwordKind)) {
                    signInSession.getMfa().setG2fa(new MfaStatusRequest(true));
                }
                if (signInSession.getMfa().isNonePassed()) {
                    redirectStrategy.sendRedirect(request, response, AuthenticationEndPointRoute.VerificationPage.PATH);
                    return;
                } else
                    addRole(user.getAuthorities());
            }
            AuthorizationRequest authorizationRequest = oAuth2RequestFactory.createAuthorizationRequest(params);
            request.getSession().setAttribute(OAuthConfig.AUTHORIZATION_REQUEST_SESSION_KEY, authorizationRequest);
            request.getSession().setAttribute(OAuthConfig.OAUTH_SIGN_IN_SESSION_KEY, signInSession);
        } catch (Exception e) {
            Logger.error("", e.getMessage());
            this.resolver.resolveException(request, response, null, e);
        }

        filterChain.doFilter(request, response);

    }
}

