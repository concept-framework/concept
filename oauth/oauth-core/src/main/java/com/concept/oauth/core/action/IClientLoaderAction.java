package com.concept.oauth.core.action;

import com.concept.oauth.core.OAuthConfig;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import com.concept.oauth.core.exception.ClientNotFoundException;
import com.concept.context.action.intefaces.IAction1;
import org.springframework.security.oauth2.provider.ClientDetailsService;

public interface IClientLoaderAction<TEntity extends IClientDetail> extends IAction1<TEntity, String>, ClientDetailsService {

    default TEntity loadClientByClientId(String var1) throws ClientNotFoundException {
        try {
            return execute(var1);
        } catch (Throwable e) {
            try {
                return execute(OAuthConfig.DEFAULT_CLIENT_ID);
            } catch (Throwable throwable) {
                throw new ClientNotFoundException(e.getMessage());
            }
        }
    }
}
