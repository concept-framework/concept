package com.concept.oauth.sql.model;

import com.concept.common.json.JsonUtil;
import com.concept.data.sql.model.BaseEntity;
import com.concept.oauth.core.metadata.PasswordKind;
import com.concept.oauth.sql.model.interfaces.IOauthClientDetail;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


@javax.persistence.Entity
@Table(name = "oauth_client_details")
public class OauthClientDetail extends BaseEntity implements IOauthClientDetail {
    @Column(name = "client_id", nullable = false, unique = true)
    private String clientId;

    @Column(name = "resource_ids")
    private String resourceIds;

    @Column(name = "client_secret")
    private String clientSecret;

    @Column(name = "scope")
    private String scope;

    @Column(name = "authorized_grant_types", nullable = false)
    private String authorizedGrantTypes;

    @Column(name = "web_server_redirect_uri")
    private String registeredRedirectUri;

    @Column(name = "authorities")
    private String authorities;

    @Column(name = "access_token_validity", nullable = false)
    private Integer accessTokenValiditySeconds;

    @Column(name = "refresh_token_validity", nullable = false)
    private Integer refreshTokenValiditySeconds;

    @Column(name = "autoapprove")
    private String autoApproveScope;

    @Column(name = "additional_information")
    private String additionalInformation;

    @ColumnDefault("'PASSWORD'")
    @Enumerated(EnumType.STRING)
    @Column(name = "password_kind", nullable = false)
    private PasswordKind passwordKind = PasswordKind.PASSWORD;

    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public Set<String> getResourceIds() {
        if (StringUtils.isEmpty(this.resourceIds)) {
            return new HashSet<>();
        } else {
            return StringUtils.commaDelimitedListToSet(this.resourceIds);
        }
    }

    @Override
    public boolean isSecretRequired() {
        return !StringUtils.isEmpty(this.clientSecret);
    }

    @Override
    public String getClientSecret() {
        return this.clientSecret;
    }

    @Override
    public boolean isScoped() {
        return this.getScope().size() > 0;
    }

    @Override
    public Set<String> getScope() {
        return StringUtils.commaDelimitedListToSet(this.scope);
    }

    @Override
    public Set<String> getAuthorizedGrantTypes() {
        return StringUtils.commaDelimitedListToSet(this.authorizedGrantTypes);
    }

    @Override
    public Set<String> getRegisteredRedirectUri() {
        return StringUtils.commaDelimitedListToSet(this.registeredRedirectUri);
    }

    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        Set<String> set = StringUtils.commaDelimitedListToSet(this.authorities);
        Set<GrantedAuthority> result = new HashSet<>();
        set.forEach(authority -> result.add(new GrantedAuthority() {
            @Override
            public String getAuthority() {
                return authority;
            }
        }));
        return result;
    }

    @Override
    public Integer getAccessTokenValiditySeconds() {
        return this.accessTokenValiditySeconds;
    }

    @Override
    public Integer getRefreshTokenValiditySeconds() {
        return this.refreshTokenValiditySeconds;
    }

    @Override
    public boolean isAutoApprove(String scope) {
        return this.getAutoApproveScope().contains(scope);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> getAdditionalInformation() {
        return JsonUtil.toObject(this.additionalInformation, Map.class);
    }

    @Override
    public Set<String> getAutoApproveScope() {
        return StringUtils.commaDelimitedListToSet(this.autoApproveScope);
    }

    @Override
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public void setResourceIds(Set<String> resourceIds) {
        this.resourceIds = StringUtils.collectionToCommaDelimitedString(resourceIds);
    }

    @Override
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public void setScope(Set<String> scope) {
        this.scope = StringUtils.collectionToCommaDelimitedString(scope);
    }

    @Override
    public void setAuthorizedGrantTypes(Set<String> authorizedGrantType) {
        this.authorizedGrantTypes = StringUtils.collectionToCommaDelimitedString(authorizedGrantType);
    }

    @Override
    public void setRegisteredRedirectUri(Set<String> registeredRedirectUriList) {
        this.registeredRedirectUri = StringUtils.collectionToCommaDelimitedString(registeredRedirectUriList);
    }

    @Override
    public void setAuthorities(Set<GrantedAuthority> authorities) {
        this.authorities = StringUtils.collectionToCommaDelimitedString(authorities);
    }

    @Override
    public void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds) {
        this.accessTokenValiditySeconds = accessTokenValiditySeconds;
    }

    @Override
    public void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds) {
        this.refreshTokenValiditySeconds = refreshTokenValiditySeconds;
    }

    @Override
    public void setAutoApproveScope(Set<String> autoApproveScope) {
        this.autoApproveScope = StringUtils.collectionToCommaDelimitedString(autoApproveScope);
    }

    @Override
    public void setAdditionalInformation(Map<String, Object> additionalInformation) {
        this.additionalInformation = JsonUtil.toString(additionalInformation);
    }

    @Override
    public PasswordKind getPasswordKind() {
        return passwordKind;
    }

    public void setPasswordKind(PasswordKind passwordKind) {
        this.passwordKind = passwordKind;
    }
}
