package com.concept.oauth.sql.action;

import com.concept.oauth.core.action.IClientLoaderAction;
import com.concept.oauth.sql.model.OauthClientDetail;
import com.concept.oauth.sql.task.GetOauthClientDetailByClientIdTask;
import com.concept.context.action.Action1;
import org.springframework.stereotype.Service;

@Service
public class GetOauthClientDetailByClientIdAction extends Action1<OauthClientDetail, String> implements IClientLoaderAction<OauthClientDetail> {

    @Override
    public OauthClientDetail execute(String clientId) throws Throwable {
        return call(GetOauthClientDetailByClientIdTask.class, clientId);
    }

}
