package com.concept.oauth.sql.task;

import com.concept.core.exception.NotFoundException;
import com.concept.oauth.sql.data.repository.IOauthClientDetailRepository;
import com.concept.oauth.sql.message.Messages;
import com.concept.oauth.sql.model.OauthClientDetail;
import com.concept.data.core.task.RepositoryTask1;
import org.springframework.stereotype.Service;

@Service
public class GetOauthClientDetailByClientIdTask extends RepositoryTask1<IOauthClientDetailRepository, OauthClientDetail, String> {

    @Override
    public OauthClientDetail execute(String clientId) throws Throwable {
        return getRepository().findByClientId(clientId).orElseThrow(() -> new NotFoundException(Messages.CLIENT_NOT_FOUND));
    }

}
