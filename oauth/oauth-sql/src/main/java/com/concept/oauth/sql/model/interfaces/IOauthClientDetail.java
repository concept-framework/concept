package com.concept.oauth.sql.model.interfaces;


import com.concept.data.sql.model.IBaseEntity;
import com.concept.oauth.core.metadata.PasswordKind;
import com.concept.oauth.core.model.interfaces.IClientDetail;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface IOauthClientDetail extends IBaseEntity, IClientDetail {

    String getClientId();

    Set<String> getResourceIds();

    boolean isSecretRequired();

    String getClientSecret();

    boolean isScoped();

    Set<String> getScope();

    Set<String> getAuthorizedGrantTypes();

    Set<String> getRegisteredRedirectUri();

    Collection<GrantedAuthority> getAuthorities();

    Integer getAccessTokenValiditySeconds();

    Integer getRefreshTokenValiditySeconds();

    boolean isAutoApprove(String scope);

    Map<String, Object> getAdditionalInformation();

    Set<String> getAutoApproveScope();

    void setClientId(String clientId);

    void setResourceIds(Set<String> resourceIds);

    void setClientSecret(String clientSecret);

    void setScope(Set<String> scope);

    void setAuthorizedGrantTypes(Set<String> authorizedGrantType);

    void setRegisteredRedirectUri(Set<String> registeredRedirectUriList);

    void setAuthorities(Set<GrantedAuthority> authorities);

    void setAccessTokenValiditySeconds(Integer accessTokenValiditySeconds);

    void setRefreshTokenValiditySeconds(Integer refreshTokenValiditySeconds);

    void setAutoApproveScope(Set<String> autoApproveScope);

    void setAdditionalInformation(Map<String, Object> additionalInformation);

    PasswordKind getPasswordKind();

    void setPasswordKind(PasswordKind passwordKind);
}
