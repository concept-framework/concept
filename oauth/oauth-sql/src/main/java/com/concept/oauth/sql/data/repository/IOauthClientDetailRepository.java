package com.concept.oauth.sql.data.repository;

import com.concept.data.sql.data.repository.IBaseRepository;
import com.concept.oauth.sql.model.OauthClientDetail;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IOauthClientDetailRepository extends IBaseRepository<OauthClientDetail> {

    Optional<OauthClientDetail> findByClientId(String clientId);

}
