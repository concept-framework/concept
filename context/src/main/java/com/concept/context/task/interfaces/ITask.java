package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess;

public interface ITask<TOutput> extends IBaseTask<TOutput>, IProcess<TOutput> {

}
