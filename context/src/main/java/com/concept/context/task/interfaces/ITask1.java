package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess1;

public interface ITask1<TOutput, TInput> extends IBaseTask<TOutput>, IProcess1<TOutput, TInput> {

}
