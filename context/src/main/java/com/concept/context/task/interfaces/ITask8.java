package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess8;

public interface ITask8<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8> extends IBaseTask<TOutput>, IProcess8<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8> {

}
