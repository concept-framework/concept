package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess7;


public interface ITask7<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7> extends IBaseTask<TOutput>, IProcess7<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7> {

}
