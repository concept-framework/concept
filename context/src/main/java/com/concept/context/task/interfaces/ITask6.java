package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess6;


public interface ITask6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> extends IBaseTask<TOutput>, IProcess6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> {

}
