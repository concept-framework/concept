package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess2;

public interface ITask2<TOutput, TInput1, TInput2> extends IBaseTask<TOutput>, IProcess2<TOutput, TInput1, TInput2> {

}
