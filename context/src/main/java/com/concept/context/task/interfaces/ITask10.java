package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess10;


public interface ITask10<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9, TInput10> extends IBaseTask<TOutput>, IProcess10<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9, TInput10> {

}
