package com.concept.context.task;

import com.concept.context.task.interfaces.ITask;

public abstract class Task<TOutput> extends BaseTask<TOutput> implements ITask<TOutput> {

    public abstract TOutput execute() throws Throwable;

}
