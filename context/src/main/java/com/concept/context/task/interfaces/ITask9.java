package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess9;


public interface ITask9<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9> extends IBaseTask<TOutput>, IProcess9<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9> {

}
