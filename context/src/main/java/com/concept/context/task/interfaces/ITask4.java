package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess4;

public interface ITask4<TOutput, TInput1, TInput2, TInput3, TInput4>  extends IBaseTask<TOutput>, IProcess4<TOutput, TInput1, TInput2, TInput3, TInput4> {

}
