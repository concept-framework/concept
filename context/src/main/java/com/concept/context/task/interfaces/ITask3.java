package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess3;

public interface ITask3<TOutput, TInput1, TInput2, TInput3> extends IBaseTask<TOutput>, IProcess3<TOutput, TInput1, TInput2, TInput3> {

}
