package com.concept.context.task.interfaces;

import com.concept.context.process.IBaseProcess;

public interface IBaseTask<TOutput> extends IBaseProcess<TOutput> {

}
