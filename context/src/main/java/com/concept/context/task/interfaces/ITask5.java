package com.concept.context.task.interfaces;

import com.concept.context.process.IProcess5;

public interface ITask5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5> extends IBaseTask<TOutput>, IProcess5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5> {

}
