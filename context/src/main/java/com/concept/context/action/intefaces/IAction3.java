package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess3;

public interface IAction3<TOutput, TInput1, TInput2, TInput3> extends IBaseAction<TOutput>, IProcess3<TOutput, TInput1, TInput2, TInput3> {

}
