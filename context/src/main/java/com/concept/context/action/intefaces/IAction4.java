package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess4;

public interface IAction4<TOutput, TInput1, TInput2, TInput3, TInput4> extends IBaseAction<TOutput>, IProcess4<TOutput, TInput1, TInput2, TInput3, TInput4> {

}
