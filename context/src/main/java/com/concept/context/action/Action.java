package com.concept.context.action;


import com.concept.context.action.intefaces.IAction;

public abstract class Action<TOutput> extends BaseAction<TOutput> implements IAction<TOutput> {

    public abstract TOutput execute() throws Throwable;

}
