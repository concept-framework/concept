package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess;

public interface IAction<TOutput> extends IBaseAction<TOutput>, IProcess<TOutput> {

}
