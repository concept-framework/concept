package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess2;

public interface IAction2<TOutput, TInput1, TInput2> extends IBaseAction<TOutput>, IProcess2<TOutput, TInput1, TInput2> {

}
