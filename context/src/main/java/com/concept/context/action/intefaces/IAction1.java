package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess1;

public interface IAction1<TOutput, TInput> extends IBaseAction<TOutput>, IProcess1<TOutput, TInput> {

}
