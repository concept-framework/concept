package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess6;

public interface IAction6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> extends IBaseAction<TOutput>, IProcess6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> {

}
