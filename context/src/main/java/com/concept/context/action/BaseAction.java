package com.concept.context.action;

import com.concept.common.mapper.Exclude;
import com.concept.common.mapper.ModelMapper;
import com.concept.common.reflection.ReflectionUtil;
import com.concept.core.application.ApplicationContextProvider;
import com.concept.context.action.intefaces.IBaseAction;
import com.concept.context.process.*;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class BaseAction<TOutput> implements IBaseAction<TOutput> {
    private DozerBeanMapper modelMapper;

    private DozerBeanMapper getModelMapper() {
        modelMapper = modelMapper == null ? new DozerBeanMapper() : modelMapper;
        return modelMapper;
    }

    private DozerBeanMapper getModelMapper(BeanMappingBuilder builder) {
        if (modelMapper == null) {
            modelMapper  = new DozerBeanMapper();
            modelMapper.addMapping(builder);
        }
        return modelMapper;
    }

    protected  <D> D plainToClass(Object source, Class<D> destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <D> D plainToClass(Object source, Class<D> destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination, excludeFields));
    }

    protected <D> D plainToClass(Object source, Class<D> destination, BeanMappingBuilder builder) {
        return getModelMapper(builder).map(source, destination);
    }

    protected <S, D> D plainToClass(S source, D destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <S, D> D plainToClass(S source, D destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination.getClass(), excludeFields));
    }

    protected <S, D> D plainToClass(S source, D destination, BeanMappingBuilder builder) {
        getModelMapper(builder).map(source, destination);
        return destination;
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination, excludeFields));
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination, BeanMappingBuilder builder) {
        final List<D> result = new ArrayList<>();
        if (source == null)
            return result;
        for (S element : source) {
            result.add(getModelMapper(builder).map(element, destination));
        }
        return result;
    }

    private <S, D> BeanMappingBuilder getDefaultMapperBuilder(Class<S> source, Class<D> destination, Set<Field> excludeFields) {
        return new BeanMappingBuilder() {
            protected void configure() {
                TypeMappingBuilder typeBuilder = mapping(source, destination, TypeMappingOptions.mapNull(false), TypeMappingOptions.mapEmptyString(false));
                if (excludeFields == null || excludeFields.isEmpty())
                    return;
                for (Field field : excludeFields) {
                    typeBuilder.exclude(field.getName());
                }
            }
        };
    }

    private Set<Field> getExcludeField(Class<?> clazz) {
        Set<Field> fields = ReflectionUtil.getFields(clazz);
        Set<Field> readOnlyFields = new HashSet<>();
        if (fields.isEmpty())
            return readOnlyFields;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Exclude.class))
                readOnlyFields.add(field);
        }
        return readOnlyFields;
    }

    protected <TProcess extends IProcess<TProcessOutput>,
            TProcessOutput>
    TProcessOutput call(Class<TProcess> process) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute();
    }

    protected <TProcess extends IProcess1<TProcessOutput, TProcessInput>,
            TProcessOutput,
            TProcessInput>
    TProcessOutput call(Class<TProcess> process, TProcessInput t1) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1);
    }

    protected <TProcess extends IProcess2<TProcessOutput, TProcessInput1, TProcessInput2>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2);
    }

    protected <TProcess extends IProcess3<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3);
    }

    protected <TProcess extends IProcess4<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4);
    }

    protected <TProcess extends IProcess5<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5);
    }

    protected <TProcess extends IProcess6<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5, TProcessInput6>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5,
            TProcessInput6>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5, TProcessInput6 t6) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5, t6);
    }

    protected <TProcess extends IProcess7<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5, TProcessInput6, TProcessInput7>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5,
            TProcessInput6,
            TProcessInput7>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5, TProcessInput6 t6, TProcessInput7 t7) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5, t6, t7);
    }

    protected <TProcess extends IProcess8<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5, TProcessInput6, TProcessInput7, TProcessInput8>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5,
            TProcessInput6,
            TProcessInput7,
            TProcessInput8>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5, TProcessInput6 t6, TProcessInput7 t7, TProcessInput8 t8) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5, t6, t7, t8);
    }

    protected <TProcess extends IProcess9<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5, TProcessInput6, TProcessInput7, TProcessInput8, TProcessInput9>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5,
            TProcessInput6,
            TProcessInput7,
            TProcessInput8,
            TProcessInput9>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5, TProcessInput6 t6, TProcessInput7 t7, TProcessInput8 t8, TProcessInput9 t9) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9);
    }

    protected <TProcess extends IProcess10<TProcessOutput, TProcessInput1, TProcessInput2, TProcessInput3, TProcessInput4, TProcessInput5, TProcessInput6, TProcessInput7, TProcessInput8, TProcessInput9, TProcessInput10>,
            TProcessOutput,
            TProcessInput1,
            TProcessInput2,
            TProcessInput3,
            TProcessInput4,
            TProcessInput5,
            TProcessInput6,
            TProcessInput7,
            TProcessInput8,
            TProcessInput9,
            TProcessInput10>
    TProcessOutput call(Class<TProcess> process, TProcessInput1 t1, TProcessInput2 t2, TProcessInput3 t3, TProcessInput4 t4, TProcessInput5 t5, TProcessInput6 t6, TProcessInput7 t7, TProcessInput8 t8, TProcessInput9 t9, TProcessInput10 t10) throws Throwable {
        return ApplicationContextProvider.context.getBean(process).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
    }
}
