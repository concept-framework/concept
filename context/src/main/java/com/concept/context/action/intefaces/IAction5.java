package com.concept.context.action.intefaces;

import com.concept.context.process.IProcess5;

public interface IAction5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5> extends IBaseAction<TOutput>, IProcess5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5> {

}
