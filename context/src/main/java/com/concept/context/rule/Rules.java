package com.concept.context.rule;

import com.concept.common.reflection.ReflectionUtil;
import com.concept.core.exception.HttpErrorMessage;
import com.concept.core.exception.HttpException;
import com.concept.core.exception.ServiceUnavailableException;
import com.concept.core.log.Logger;
import com.concept.core.payload.IPayload;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;


@Documented
@Constraint(validatedBy = Rules.RuleValidator.class)
@Target({ElementType.CONSTRUCTOR, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Rules {
    String message() default "";

    Class<? extends ConstraintRule>[] constraints() default {};

    Class<? extends DerivationRule>[] derivations() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class RuleValidator implements ConstraintValidator<Rules, IPayload> {
        private Rules annotation;

        @Override
        public void initialize(Rules annotation) {
            this.annotation = annotation;
        }

        @Override
        public boolean isValid(IPayload field, ConstraintValidatorContext cxt) {
            boolean isValidConstraintRules = validationConstraintRule(field, cxt);
            boolean isValidDerivationRule = validationDerivationRule(field, cxt);
            return isValidConstraintRules && isValidDerivationRule;
        }

        private boolean validationDerivationRule(IPayload field, ConstraintValidatorContext cxt) {
            if (annotation == null || annotation.derivations().length <= 0)
                return true;
            for (Class rule : annotation.derivations()) {
                try {
                    DerivationRule<IPayload> instance = ((DerivationRule<IPayload>) rule.getDeclaredConstructor().newInstance());
                    if (!instance.isEnable() && instance.getFields() == null || instance.getFields().isEmpty())
                        continue;
                    for (String fieldName : instance.getFields()) {
                        if (instance.condition(field))
                            ReflectionUtil.setPropertyValue(field, fieldName, instance.getDerivedValue(field));
                    }

                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                    String message = (rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim();
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(new ServiceUnavailableException(message).toString()).addConstraintViolation();
                    return false;
                } catch (Throwable e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String message = StringUtils.EMPTY;
                    if (e instanceof HttpException) {
                        message = e.toString();
                    }
                    if (StringUtils.isBlank(message)) {
                        String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                        message = new ServiceUnavailableException((rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim()).toString();
                    }
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                    return false;
                }
            }
            return true;
        }

        private boolean validationConstraintRule(IPayload field, ConstraintValidatorContext cxt) {
            if (annotation == null || annotation.constraints().length <= 0)
                return true;
            return isValidRule(annotation.constraints(), field, cxt);
        }

        private boolean isValidRule(Class<? extends ConstraintRule>[] rules, IPayload field, ConstraintValidatorContext cxt) {
            for (Class rule : rules) {
                try {
                    ConstraintRule<IPayload> instance = ((ConstraintRule<IPayload>) rule.getDeclaredConstructor().newInstance());
                    if (instance.isEnable() && instance.condition(field)) {
                        String message = instance.message(field);
                        if (StringUtils.isEmpty(message)) {
                            String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                            message = (rule_name.toUpperCase() + "_FAILED").trim();
                        }
                        cxt.disableDefaultConstraintViolation();
                        cxt.buildConstraintViolationWithTemplate(new HttpException(new HttpErrorMessage(instance.httpStatus(), message)).toString()).addConstraintViolation();
                        return false;
                    }
                } catch (InvocationTargetException | InstantiationException | NoSuchMethodException | IllegalAccessException e) {
                    Logger.error("ValidationConstraintRulesException", e.getMessage());
                    String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                    String message = (rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim();
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(new ServiceUnavailableException(message).toString()).addConstraintViolation();
                    return false;
                } catch (Throwable e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String message = StringUtils.EMPTY;
                    if (e instanceof HttpException) {
                        message = e.toString();
                    }
                    if (StringUtils.isBlank(message)) {
                        String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                        message = new ServiceUnavailableException((rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim()).toString();
                    }
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                    return false;
                }
            }
            return true;
        }

        private HttpException extractMessage(HttpException exception, String defaultMessage) {
            if (exception == null || exception.getErrorMessage() == null) {
                return new ServiceUnavailableException(defaultMessage);
            }
            return new HttpException(exception.getErrorMessage());
        }
    }
}
