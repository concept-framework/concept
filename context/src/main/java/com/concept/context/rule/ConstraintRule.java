package com.concept.context.rule;

import com.concept.core.application.ApplicationContextProvider;
import com.concept.core.exception.HttpException;
import com.concept.core.payload.IPayload;
import com.concept.context.action.*;
import org.springframework.http.HttpStatus;

public abstract class ConstraintRule<TPayload extends IPayload> extends Rule<TPayload> {

    public abstract String message(TPayload t);

    public HttpStatus httpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    public boolean isEnable() {
        return true;
    }

    protected <TAction extends Action<TOutput>, TOutput>
    TOutput call(Class<TAction> action) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute();
    }

    protected <TAction extends Action1<TOutput, TInput1>,
            TOutput,
            TInput1>
    TOutput call(Class<TAction> action, TInput1 t1) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1);
    }

    protected <TAction extends Action2<TOutput, TInput1, TInput2>,
            TOutput,
            TInput1,
            TInput2,
            TException extends HttpException>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2);
    }

    protected <TAction extends Action3<TOutput, TInput1, TInput2, TInput3>,
            TOutput,
            TInput1,
            TInput2,
            TInput3>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3);
    }

    protected <TAction extends Action4<TOutput, TInput1, TInput2, TInput3, TInput4>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4);
    }

    protected <TAction extends Action5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5);
    }

    protected <TAction extends Action6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6);
    }

    protected <TAction extends Action7<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7);
    }

    protected <TAction extends Action8<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8);
    }

    protected <TAction extends Action9<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8,
            TInput9>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8, TInput9 t9) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9);
    }

    protected <TAction extends Action10<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9, TInput10>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8,
            TInput9,
            TInput10>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8, TInput9 t9, TInput10 t10) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
    }

}
