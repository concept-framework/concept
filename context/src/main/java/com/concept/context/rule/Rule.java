package com.concept.context.rule;

import java.io.Serializable;

public abstract class Rule<TMessage extends Serializable> {

    public abstract boolean condition(TMessage t) throws Throwable;

}
