package com.concept.context.job;

import com.concept.common.mapper.Exclude;
import com.concept.common.reflection.ReflectionUtil;
import com.concept.context.action.*;
import com.concept.core.application.ApplicationContextProvider;
import com.concept.core.exception.HttpException;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.TypeMappingBuilder;
import org.dozer.loader.api.TypeMappingOptions;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Job {

    private ExecutorService executorService;

    protected ExecutorService getExecutorService() {
        return getExecutorService(10);
    }

    protected ExecutorService getExecutorService(int poolCount) {
        return executorService = executorService == null ? Executors.newFixedThreadPool(poolCount) : executorService;
    }

    private DozerBeanMapper modelMapper;

    private DozerBeanMapper getModelMapper() {
        modelMapper = modelMapper == null ? new DozerBeanMapper() : modelMapper;
        return modelMapper;
    }

    private DozerBeanMapper getModelMapper(BeanMappingBuilder builder) {
        if (modelMapper == null) {
            modelMapper  = new DozerBeanMapper();
            modelMapper.addMapping(builder);
        }
        return modelMapper;
    }

    protected  <D> D plainToClass(Object source, Class<D> destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <D> D plainToClass(Object source, Class<D> destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination, excludeFields));
    }

    protected <D> D plainToClass(Object source, Class<D> destination, BeanMappingBuilder builder) {
        return getModelMapper(builder).map(source, destination);
    }

    protected <S, D> D plainToClass(S source, D destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <S, D> D plainToClass(S source, D destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination.getClass(), excludeFields));
    }

    protected <S, D> D plainToClass(S source, D destination, BeanMappingBuilder builder) {
        getModelMapper(builder).map(source, destination);
        return destination;
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination) {
        return plainToClass(source, destination, getExcludeField(source.getClass()));
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination, Set<Field> excludeFields) {
        return plainToClass(source, destination, getDefaultMapperBuilder(source.getClass(), destination, excludeFields));
    }

    protected <S, D> List<D> plainToClass(Iterable<S> source, final Class<D> destination, BeanMappingBuilder builder) {
        final List<D> result = new ArrayList<>();
        if (source == null)
            return result;
        for (S element : source) {
            result.add(getModelMapper(builder).map(element, destination));
        }
        return result;
    }

    private <S, D> BeanMappingBuilder getDefaultMapperBuilder(Class<S> source, Class<D> destination, Set<Field> excludeFields) {
        return new BeanMappingBuilder() {
            protected void configure() {
                TypeMappingBuilder typeBuilder = mapping(source, destination, TypeMappingOptions.mapNull(false), TypeMappingOptions.mapEmptyString(false));
                if (excludeFields == null || excludeFields.isEmpty())
                    return;
                for (Field field : excludeFields) {
                    typeBuilder.exclude(field.getName());
                }
            }
        };
    }

    private Set<Field> getExcludeField(Class<?> clazz) {
        Set<Field> fields = ReflectionUtil.getFields(clazz);
        Set<Field> readOnlyFields = new HashSet<>();
        if (fields.isEmpty())
            return readOnlyFields;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Exclude.class))
                readOnlyFields.add(field);
        }
        return readOnlyFields;
    }

    protected <TAction extends Action<TOutput>, TOutput>
    TOutput call(Class<TAction> action) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute();
    }

    protected <TAction extends Action1<TOutput, TInput1>,
            TOutput,
            TInput1>
    TOutput call(Class<TAction> action, TInput1 t1) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1);
    }

    protected <TAction extends Action2<TOutput, TInput1, TInput2>,
            TOutput,
            TInput1,
            TInput2,
            TException extends HttpException>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2);
    }

    protected <TAction extends Action3<TOutput, TInput1, TInput2, TInput3>,
            TOutput,
            TInput1,
            TInput2,
            TInput3>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3);
    }

    protected <TAction extends Action4<TOutput, TInput1, TInput2, TInput3, TInput4>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4);
    }

    protected <TAction extends Action5<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5);
    }

    protected <TAction extends Action6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6);
    }

    protected <TAction extends Action7<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7);
    }

    protected <TAction extends Action8<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8);
    }

    protected <TAction extends Action9<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8,
            TInput9>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8, TInput9 t9) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9);
    }

    protected <TAction extends Action10<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8, TInput9, TInput10>,
            TOutput,
            TInput1,
            TInput2,
            TInput3,
            TInput4,
            TInput5,
            TInput6,
            TInput7,
            TInput8,
            TInput9,
            TInput10>
    TOutput call(Class<TAction> action, TInput1 t1, TInput2 t2, TInput3 t3, TInput4 t4, TInput5 t5, TInput6 t6, TInput7 t7, TInput8 t8, TInput9 t9, TInput10 t10) throws Throwable {
        return ApplicationContextProvider.context.getBean(action).execute(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);
    }

}
