package com.concept.context.process;

public interface IProcess<TOutput> extends IBaseProcess<TOutput> {

    TOutput execute() throws Throwable;

}
