package com.concept.context.process;

public interface IProcess1<TOutput, TInput> extends IBaseProcess<TOutput> {

    TOutput execute(TInput param) throws Throwable;

}
