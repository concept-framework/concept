package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class GatewayTimeoutException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.GATEWAY_TIMEOUT;
    }

    public GatewayTimeoutException(MessageHandler message) {
        super(message);
    }

    public GatewayTimeoutException(String message) {
        super(message);
    }

    public GatewayTimeoutException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.GATEWAY_TIMEOUT, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
