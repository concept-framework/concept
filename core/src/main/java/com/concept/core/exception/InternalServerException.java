package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class InternalServerException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    public InternalServerException(MessageHandler message) {
        super(message);
    }

    public InternalServerException(String message) {
        super(message);
    }

    public InternalServerException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
