package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class RequestTimeoutException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.REQUEST_TIMEOUT;
    }

    public RequestTimeoutException(MessageHandler message) {
        super(message);
    }

    public RequestTimeoutException(String message) {
        super(message);
    }

    public RequestTimeoutException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.REQUEST_TIMEOUT, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
