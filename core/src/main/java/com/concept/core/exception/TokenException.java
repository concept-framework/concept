package com.concept.core.exception;


import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class TokenException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.UNAUTHORIZED;
    }

    public TokenException(MessageHandler message) {
        super(message);
    }

    public TokenException(String message) {
        super(message);
    }

    public TokenException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.UNAUTHORIZED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
