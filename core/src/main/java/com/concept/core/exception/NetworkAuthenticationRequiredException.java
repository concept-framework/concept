package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class NetworkAuthenticationRequiredException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NETWORK_AUTHENTICATION_REQUIRED;
    }

    public NetworkAuthenticationRequiredException(MessageHandler message) {
        super(message);
    }

    public NetworkAuthenticationRequiredException(String message) {
        super(message);
    }

    public NetworkAuthenticationRequiredException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }

}
