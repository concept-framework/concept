package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class PayloadTooLargeException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.PAYLOAD_TOO_LARGE;
    }

    public PayloadTooLargeException(MessageHandler message) {
        super(message);
    }

    public PayloadTooLargeException(String message) {
        super(message);
    }

    public PayloadTooLargeException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.PAYLOAD_TOO_LARGE, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
