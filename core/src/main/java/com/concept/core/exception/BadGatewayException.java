package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class BadGatewayException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_GATEWAY;
    }

    public BadGatewayException(MessageHandler message) {
        super(message);
    }

    public BadGatewayException(String message) {
        super(message);
    }

    public BadGatewayException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.BAD_GATEWAY, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
