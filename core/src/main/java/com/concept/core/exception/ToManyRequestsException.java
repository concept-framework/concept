package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class ToManyRequestsException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.TOO_MANY_REQUESTS;
    }

    public ToManyRequestsException(MessageHandler message) {
        super(message);
    }

    public ToManyRequestsException(String message) {
        super(message);
    }

    public ToManyRequestsException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.TOO_MANY_REQUESTS, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}