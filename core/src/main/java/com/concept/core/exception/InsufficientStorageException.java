package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class InsufficientStorageException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.INSUFFICIENT_STORAGE;
    }

    public InsufficientStorageException(MessageHandler message) {
        super(message);
    }

    public InsufficientStorageException(String message) {
        super(message);
    }

    public InsufficientStorageException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.INSUFFICIENT_STORAGE, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }

}
