package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class ProcessingException  extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.PROCESSING;
    }

    public ProcessingException(MessageHandler message) {
        super(message);
    }

    public ProcessingException(String message) {
        super(message);
    }

    public ProcessingException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.PROCESSING, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}