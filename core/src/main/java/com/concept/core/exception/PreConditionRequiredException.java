package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class PreConditionRequiredException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.PRECONDITION_REQUIRED;
    }

    public PreConditionRequiredException(MessageHandler message) {
        super(message);
    }

    public PreConditionRequiredException(String message) {
        super(message);
    }

    public PreConditionRequiredException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.PRECONDITION_REQUIRED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
