package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class UnSupportMediaTypeException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.UNSUPPORTED_MEDIA_TYPE;
    }

    public UnSupportMediaTypeException(MessageHandler message) {
        super(message);
    }

    public UnSupportMediaTypeException(String message) {
        super(message);
    }

    public UnSupportMediaTypeException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.UNSUPPORTED_MEDIA_TYPE, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
