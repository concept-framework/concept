package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class BadRequestException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.BAD_REQUEST;
    }

    public BadRequestException(MessageHandler message) {
        super(message);
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.BAD_REQUEST, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
