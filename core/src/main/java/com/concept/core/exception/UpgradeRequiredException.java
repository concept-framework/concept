package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class UpgradeRequiredException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.UPGRADE_REQUIRED;
    }

    public UpgradeRequiredException(MessageHandler message) {
        super(message);
    }

    public UpgradeRequiredException(String message) {
        super(message);
    }

    public UpgradeRequiredException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.UPGRADE_REQUIRED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}