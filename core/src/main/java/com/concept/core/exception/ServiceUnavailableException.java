package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class ServiceUnavailableException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.SERVICE_UNAVAILABLE;
    }

    public ServiceUnavailableException(MessageHandler message) {
        super(message);
    }

    public ServiceUnavailableException(String message) {
        super(message);
    }

    public ServiceUnavailableException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.SERVICE_UNAVAILABLE, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
