package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class NotImplementedException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_IMPLEMENTED;
    }

    public NotImplementedException(MessageHandler message) {
        super(message);
    }

    public NotImplementedException(String message) {
        super(message);
    }

    public NotImplementedException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.NOT_IMPLEMENTED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
