package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class ForbiddenException  extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.FORBIDDEN;
    }

    public ForbiddenException(MessageHandler message) {
        super(message);
    }

    public ForbiddenException(String message) {
        super(message);
    }

    public ForbiddenException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.FORBIDDEN, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}