package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class PaymentRequiredException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.PAYMENT_REQUIRED;
    }

    public PaymentRequiredException(MessageHandler message) {
        super(message);
    }

    public PaymentRequiredException(String message) {
        super(message);
    }

    public PaymentRequiredException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.PAYMENT_REQUIRED, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
