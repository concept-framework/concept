package com.concept.core.exception;

import com.concept.core.message.MessageHandler;
import org.springframework.http.HttpStatus;

public class NotFoundException extends HttpException {
    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.NOT_FOUND;
    }

    public NotFoundException(MessageHandler message) {
        super(message);
    }

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException(ErrorMessage errorMessage) {
        super(new HttpErrorMessage(HttpStatus.NOT_FOUND, errorMessage.getMessage(), errorMessage.getStatus(), errorMessage.getPath(), errorMessage.getTimestamp()));
    }
}
