package com.concept.common.number;

import java.util.Random;

public class NumberUtils {

    public static String getRandom(int length) {
        if (length < 1 || length > 12) {
            length = 6;
        }
        Random random = new Random();
        long nextLong = Math.abs(random.nextLong());
        return String.valueOf(nextLong).substring(0, length);
    }
}
