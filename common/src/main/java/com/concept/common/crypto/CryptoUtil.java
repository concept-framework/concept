package com.concept.common.crypto;

import com.concept.common.string.StringUtil;
import com.concept.core.log.Logger;
import org.apache.commons.lang3.StringUtils;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoUtil {

    public static String sha256(String message) {
        return sha256(message.getBytes(StandardCharsets.UTF_8));
    }

    public static String sha256(byte[] message) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(message);
            return StringUtil.encodeHexString(hash);
        } catch (NoSuchAlgorithmException e) {
            Logger.error("sha256", e.getMessage());
            return StringUtils.EMPTY;
        }
    }

    public static String md5(String message) {
        return md5(message.getBytes(StandardCharsets.UTF_8));
    }

    public static String md5(byte[] message) {
        try {
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(message);
            byte[] digest = m.digest();
            return StringUtil.encodeHexString(digest);
        } catch (NoSuchAlgorithmException e) {
            Logger.error("md5", e.getMessage());
            return "";
        }
    }
}
