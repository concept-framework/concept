package com.concept.common.phone;

import com.concept.common.text.TextUtil;
import com.concept.core.log.Logger;
import com.concept.core.AppInfo;
import com.concept.core.metadata.Country;
import com.google.i18n.phonenumbers.*;

public class PhoneUtil {

    public static Phone parse(String phone) {
        return parse(phone, AppInfo.DEFAULT_COUNTRY);
    }

    public static Phone parse(String phone, Country defaultCountry) {
        phone = TextUtil.toEnglishNumber(phone);
        try {
            defaultCountry = defaultCountry == null ? AppInfo.DEFAULT_COUNTRY : defaultCountry;
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phone, defaultCountry.name());
            int countryCode = numberProto.getCountryCode();
            long nationalNumber = numberProto.getNationalNumber();
            return new Phone(countryCode, nationalNumber);
        } catch (NumberParseException e) {
            Logger.error("PhoneParseException", e.getMessage());
            return new Phone();
        }
    }

    public static String toHide(String phone) {
        if (phone == null)
            phone = "";
        if (phone.length() > 7)
            return "\u200E" + phone.substring(0, 7) + "****" + "\u200E";
        return phone;
    }

    public static boolean isValid(String phoneNumber) {
        return isValid(phoneNumber, AppInfo.DEFAULT_COUNTRY);
    }

    public static boolean isValid(String phoneNumber, Country... countries) {
        try {
            if (countries == null || countries.length <= 0)
                countries = new Country[]{AppInfo.DEFAULT_COUNTRY};
            for (Country country : countries) {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                Phonenumber.PhoneNumber pn = phoneUtil.parse(phoneNumber, Country.IR.name());
                if (phoneUtil.isValidNumberForRegion(pn, country.name()))
                    return true;
            }
            return false;
        } catch (NumberParseException e) {
            Logger.error("PhoneParseException", e.getMessage());
            return false;
        }
    }
}
