package com.concept.common.generic;

public interface IComparable<T> {

    T value();

    default boolean isIn(T... values) {
        for (T current : values) {
            if (value().equals(current)) {
                return true;
            }
        }
        return false;
    }

    default boolean notEqual(T value) {
        return !equals(value);
    }

    default boolean notIn(T... values) {
        for (T current : values) {
            if (value().equals(current)) {
                return false;
            }
        }
        return true;
    }

    default boolean equalAll(T... values) {
        for (T current : values) {
            if (!value().equals(current)) {
                return false;
            }
        }
        return true;
    }
}
