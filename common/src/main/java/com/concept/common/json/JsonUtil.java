package com.concept.common.json;

import com.concept.core.exception.*;
import com.concept.core.log.Logger;
import com.concept.core.message.Messages;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


public class JsonUtil {

    public static String toString(Object value) {
        try {
            ObjectMapper Obj = new ObjectMapper();
            return Obj.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            Logger.error("JsonProcessingException", e.getMessage());
            return "";
        }
    }

    public static <T> T toObject(String value, Class<T> valueType) {
        try {
            ObjectMapper Obj = new ObjectMapper();
            Obj.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return Obj.readValue(value, valueType);
        } catch (IOException e) {
            Logger.error("JsonParseException", e.getMessage());
            return null;
        }
    }

    public static <T> T toObject(Object value, Class<T> valueType) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.convertValue(value, valueType);
        } catch (Exception e) {
            Logger.error("JsonParseException", e.getMessage());
            return null;
        }
    }

    public static <T> void writeToFile(String path, T value) throws NotAcceptableException {
        try {
            File file = new File(path);
            File dir = file.getParentFile();
            if (!dir.exists())
                dir.mkdirs();
            String json = toString(value);
            Files.write(Paths.get(path), json.getBytes());
        } catch (IOException e) {
            Logger.error("Write file", e.getMessage());
            throw new NotAcceptableException(Messages.ERROR_ON_SAVE_FILE);
        }
    }

    public static <T> T readFromFile(String path, Class<T> valueType) throws NotFoundException, NotAcceptableException {
        File file = new File(path);
        if (!file.exists() || !file.isFile())
            throw new NotFoundException(Messages.FILE_NOT_FOUND);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(file, valueType);
        } catch (IOException e) {
            Logger.error("Read file", e.getMessage());
            throw new NotAcceptableException(Messages.ERROR_ON_READ_FILE);
        }
    }
}
