package com.concept.data.mongo.data.criteria;

import com.concept.data.core.data.criteria.ICoreCriteria;
import com.concept.data.mongo.model.IBaseDocument;
import org.springframework.stereotype.Repository;

@Repository
public interface IMongoCriteria<TDocument extends IBaseDocument> extends ICoreCriteria<TDocument> {

}
