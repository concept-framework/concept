package com.concept.data.mongo.data.repository;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.data.mongo.model.IBaseDocument;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IMongoRepository<TDocument extends IBaseDocument> extends ICoreRepository<TDocument> {

}
