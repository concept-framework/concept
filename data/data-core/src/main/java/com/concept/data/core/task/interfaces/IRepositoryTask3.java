package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask3;

public interface IRepositoryTask3<TRepository extends ICoreRepository, TOutput, TInput1, TInput2, TInput3> extends IBaseRepositoryTask<TRepository, TOutput>, ITask3<TOutput, TInput1, TInput2, TInput3> {

}
