package com.concept.data.core.data.repository;

import com.concept.data.core.model.ICoreEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;


@NoRepositoryBean
public interface ICoreRepository<TEntity extends ICoreEntity> extends PagingAndSortingRepository<TEntity, String> {

}
