package com.concept.data.core.data.criteria;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.data.core.model.ICoreEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@NoRepositoryBean
public interface ICoreCriteria<TEntity extends ICoreEntity> extends ICoreRepository<TEntity> {

}
