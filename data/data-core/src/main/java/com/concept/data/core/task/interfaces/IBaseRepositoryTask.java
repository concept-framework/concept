package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.IBaseTask;

public interface IBaseRepositoryTask<TRepository extends ICoreRepository, TOutput> extends IBaseTask<TOutput> {

}
