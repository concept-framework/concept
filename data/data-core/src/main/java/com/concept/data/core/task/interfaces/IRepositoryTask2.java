package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask2;

public interface IRepositoryTask2<TRepository extends ICoreRepository, TOutput, TInput1, TInput2> extends IBaseRepositoryTask<TRepository, TOutput>, ITask2<TOutput, TInput1, TInput2> {

}
