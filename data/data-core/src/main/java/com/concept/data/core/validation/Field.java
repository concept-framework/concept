package com.concept.data.core.validation;

import com.concept.validator.Length;

import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Field {
    String value() default "";

    boolean required() default false;

    boolean visible() default true;

    boolean readOnly() default false;

    Length length();


}
