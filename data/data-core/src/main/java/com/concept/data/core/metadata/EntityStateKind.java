package com.concept.data.core.metadata;

import com.concept.common.generic.IComparable;

public enum EntityStateKind implements IComparable<EntityStateKind> {
    PERSIST,
    UPDATE,
    REMOVE,
    LOAD,
    ALL;

    @Override
    public EntityStateKind value() {
        return this;
    }
}
