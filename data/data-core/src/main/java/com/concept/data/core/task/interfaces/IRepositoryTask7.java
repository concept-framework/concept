package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask7;


public interface IRepositoryTask7<TRepository extends ICoreRepository, TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7> extends IBaseRepositoryTask<TRepository, TOutput>, ITask7<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7> {

}
