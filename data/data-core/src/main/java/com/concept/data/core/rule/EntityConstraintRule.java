package com.concept.data.core.rule;

import com.concept.context.rule.ConstraintRule;
import com.concept.data.core.metadata.EntityStateKind;
import com.concept.data.core.model.ICoreEntity;

import java.util.Set;

public abstract class EntityConstraintRule<TEntity extends ICoreEntity> extends ConstraintRule<TEntity> {

    EntityStateKind entityState;

    public EntityStateKind getEntityState() {
        return entityState;
    }

    @Override
    public boolean isEnable() {
        return getEntityState() != null && getEntityState().isIn(EntityStateKind.PERSIST,EntityStateKind.UPDATE, EntityStateKind.REMOVE);
    }
}
