package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask;

public interface IRepositoryTask<TRepository extends ICoreRepository, TOutput> extends IBaseRepositoryTask<TRepository, TOutput>, ITask<TOutput> {

}
