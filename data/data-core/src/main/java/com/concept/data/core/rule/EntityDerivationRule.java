package com.concept.data.core.rule;

import com.concept.context.rule.DerivationRule;
import com.concept.data.core.metadata.EntityStateKind;
import com.concept.data.core.model.ICoreEntity;
import de.cronn.reflection.util.PropertyGetter;
import java.util.Set;

public abstract class EntityDerivationRule<TEntity extends ICoreEntity> extends DerivationRule<TEntity> {

    EntityStateKind entityState;

    public EntityDerivationRule(PropertyGetter<TEntity> field) {
        super(field);
    }

    public EntityDerivationRule(Set<PropertyGetter<TEntity>> fields) {
        super(fields);
    }

    public EntityStateKind getEntityState() {
        return entityState;
    }

    @Override
    public boolean isEnable() {
        return getEntityState() != null && getEntityState().isIn(EntityStateKind.PERSIST,EntityStateKind.UPDATE, EntityStateKind.REMOVE);
    }
}
