package com.concept.data.core.rule;

import com.concept.common.reflection.ReflectionUtil;
import com.concept.core.exception.HttpErrorMessage;
import com.concept.core.exception.HttpException;
import com.concept.core.exception.ServiceUnavailableException;
import com.concept.core.log.Logger;
import com.concept.data.core.metadata.EntityStateKind;
import com.concept.data.core.model.ICoreEntity;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;


@Documented
@Constraint(validatedBy = EntityRules.RuleValidator.class)
@Target({ElementType.CONSTRUCTOR, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityRules {
    String message() default "";

    Class<? extends EntityConstraintRule>[] constraints() default {};

    Class<? extends EntityDerivationRule>[] derivations() default {};

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    class RuleValidator implements ConstraintValidator<EntityRules, ICoreEntity> {
        private EntityRules annotation;

        @Override
        public void initialize(EntityRules annotation) {
            this.annotation = annotation;
        }

        @Override
        public boolean isValid(ICoreEntity field, ConstraintValidatorContext cxt) {
            boolean isValidConstraintRules = validationConstraintRule(field, cxt);
            boolean isValidDerivationRule = validationDerivationRule(field, cxt);
            return isValidConstraintRules && isValidDerivationRule;
        }

        private boolean validationDerivationRule(ICoreEntity entity, ConstraintValidatorContext cxt) {
            if (annotation == null || annotation.derivations().length <= 0)
                return true;
            for (Class rule : annotation.derivations()) {
                try {
                    EntityStateKind entityState = EntityStateKind.ALL;
                    if (cxt instanceof HibernateConstraintValidatorContext) {
                        entityState = cxt.unwrap(HibernateConstraintValidatorContext.class).getConstraintValidatorPayload(EntityStateKind.class);
                        entityState = entityState == null ? EntityStateKind.ALL : entityState;
                    }
                    EntityDerivationRule<ICoreEntity> instance = ((EntityDerivationRule<ICoreEntity>) rule.getDeclaredConstructor().newInstance());
                    if (instance.getFields() == null || instance.getFields().isEmpty())
                        continue;
                    instance.entityState = entityState;
                    if (instance.isEnable() && instance.condition(entity)) {
                        for (String fieldName : instance.getFields()) {
                            ReflectionUtil.setPropertyValue(entity, fieldName, instance.getDerivedValue(entity));
                        }
                    }

                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                    String message = (rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim();
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(new ServiceUnavailableException(message).toString()).addConstraintViolation();
                    return false;
                } catch (Throwable e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String message = StringUtils.EMPTY;
                    if (e instanceof HttpException) {
                        message = e.toString();
                    }
                    if (StringUtils.isBlank(message)) {
                        String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                        message = new ServiceUnavailableException((rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim()).toString();
                    }
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                    return false;
                }
            }
            return true;
        }

        private boolean validationConstraintRule(ICoreEntity field, ConstraintValidatorContext cxt) {
            if (annotation == null || annotation.constraints().length <= 0)
                return true;
            return isValidRule(annotation.constraints(), field, cxt);
        }

        private boolean isValidRule(Class<? extends EntityConstraintRule>[] rules, ICoreEntity entity, ConstraintValidatorContext cxt) {
            for (Class rule : rules) {
                try {
                    EntityStateKind entityState = EntityStateKind.ALL;
                    if (cxt instanceof HibernateConstraintValidatorContext) {
                        entityState = cxt.unwrap(HibernateConstraintValidatorContext.class).getConstraintValidatorPayload(EntityStateKind.class);
                        entityState = entityState == null ? EntityStateKind.ALL : entityState;
                    }
                    EntityConstraintRule<ICoreEntity> instance = ((EntityConstraintRule<ICoreEntity>) rule.getDeclaredConstructor().newInstance());
                    instance.entityState = entityState;
                    if (instance.isEnable() && instance.condition(entity)) {
                        String message = instance.message(entity);
                        if (StringUtils.isEmpty(message)) {
                            String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                            message = (rule_name.toUpperCase() + "_FAILED").trim();
                        }
                        cxt.disableDefaultConstraintViolation();
                        cxt.buildConstraintViolationWithTemplate(new HttpException(new HttpErrorMessage(instance.httpStatus(), message)).toString()).addConstraintViolation();
                        return false;
                    }
                } catch (InvocationTargetException | InstantiationException | NoSuchMethodException | IllegalAccessException e) {
                    Logger.error("ValidationConstraintRulesException", e.getMessage());
                    String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                    String message = (rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim();
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(new ServiceUnavailableException(message).toString()).addConstraintViolation();
                    return false;
                } catch (Throwable e) {
                    Logger.error("ValidationDerivationRulesException", e.getMessage());
                    String message = StringUtils.EMPTY;
                    if (e instanceof HttpException) {
                        message = e.toString();
                    }
                    if (StringUtils.isBlank(message)) {
                        String rule_name = StringUtils.uncapitalize(rule.getSimpleName()).replaceAll("([A-Z])", "_$1");
                        message = new ServiceUnavailableException((rule_name.toUpperCase() + "_STRUCTURE_HAS_ERROR").trim()).toString();
                    }
                    cxt.disableDefaultConstraintViolation();
                    cxt.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                    return false;
                }
            }
            return true;
        }
    }
}
