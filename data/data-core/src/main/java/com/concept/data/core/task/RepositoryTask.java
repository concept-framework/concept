package com.concept.data.core.task;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.data.core.task.interfaces.IRepositoryTask;

public abstract class RepositoryTask<TRepository extends ICoreRepository, TOutput> extends BaseRepositoryTask<TRepository, TOutput> implements IRepositoryTask<TRepository, TOutput> {

    public abstract TOutput execute() throws Throwable;

}
