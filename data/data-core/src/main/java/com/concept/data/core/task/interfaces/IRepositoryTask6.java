package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask6;


public interface IRepositoryTask6<TRepository extends ICoreRepository, TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> extends IBaseRepositoryTask<TRepository, TOutput>, ITask6<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6> {

}
