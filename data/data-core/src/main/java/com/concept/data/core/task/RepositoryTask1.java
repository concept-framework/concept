package com.concept.data.core.task;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask1;

public abstract class RepositoryTask1<TRepository extends ICoreRepository, TOutput, TInput> extends BaseRepositoryTask<TRepository, TOutput> implements ITask1<TOutput, TInput> {

    public abstract TOutput execute(TInput t1) throws Throwable;

}
