package com.concept.data.core.model;

import com.concept.core.payload.IPayload;

import java.io.Serializable;
import java.util.Date;

public interface ICoreEntity extends IPayload {
    String getId();

    void setId(String id);

    Date getCreated();

    void setCreated(Date created);

    Date getModified();

    void setModified(Date modified);

    Long getVersion();

    void setVersion(Long version);

}
