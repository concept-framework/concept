package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask1;

public interface IRepositoryTask1<TRepository extends ICoreRepository, TOutput, TInput> extends IBaseRepositoryTask<TRepository, TOutput>, ITask1<TOutput, TInput> {

}
