package com.concept.data.core.task;

import com.concept.core.application.ApplicationContextProvider;
import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.BaseTask;
import com.concept.data.core.task.interfaces.IBaseRepositoryTask;

import java.lang.reflect.ParameterizedType;

public abstract class BaseRepositoryTask<TRepository extends ICoreRepository, TOutput>  extends BaseTask<TOutput> implements IBaseRepositoryTask<TRepository, TOutput> {

    protected TRepository getRepository() {
        return ApplicationContextProvider.context.getBean((Class<TRepository>) ((ParameterizedType) (getClass().getGenericSuperclass())).getActualTypeArguments()[0]);
    }

}
