package com.concept.data.core.task.interfaces;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.context.task.interfaces.ITask8;

public interface IRepositoryTask8<TRepository extends ICoreRepository, TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8> extends IBaseRepositoryTask<TRepository, TOutput>, ITask8<TOutput, TInput1, TInput2, TInput3, TInput4, TInput5, TInput6, TInput7, TInput8> {

}
