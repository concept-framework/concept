package com.concept.data.sql.data.repository;

import com.concept.data.core.data.repository.ICoreRepository;
import com.concept.data.sql.model.IBaseEntity;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IBaseRepository<TEntity extends IBaseEntity> extends ICoreRepository<TEntity> {

}
