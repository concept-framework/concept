package com.concept.data.sql.data.criteria;

import com.concept.data.sql.model.IBaseEntity;

import javax.persistence.EntityManager;

public abstract class BaseCriteria<TEntity extends IBaseEntity> implements IBaseCriteria<TEntity> {
    private final EntityManager entityManager;

    public BaseCriteria(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
