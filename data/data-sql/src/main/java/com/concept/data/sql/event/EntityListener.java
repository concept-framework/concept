package com.concept.data.sql.event;

import com.concept.data.core.metadata.EntityStateKind;
import com.concept.data.sql.payload.InProgressValidation;
import org.hibernate.validator.HibernateValidatorFactory;

import javax.persistence.*;
import javax.validation.*;
import java.util.HashSet;
import java.util.Set;

public class EntityListener {

    static Set<InProgressValidation> inProgressValidations = new HashSet<>();

    @PrePersist
    private void prePersist(Object entity) {
        validation(entity, EntityStateKind.PERSIST);
    }

    @PostPersist
    private void postPersist(Object entity) {

    }

    @PreUpdate
    private void preUpdate(Object entity) {
        validation(entity, EntityStateKind.UPDATE);
    }

    @PostUpdate
    private void postUpdate(Object entity) {

    }

    @PreRemove
    private void preRemove(Object entity) {
        validation(entity, EntityStateKind.REMOVE);
    }

    @PostRemove
    private void postRemove(Object entity) {

    }

    @PostLoad
    private void postLoad(Object entity) {
        validation(entity, EntityStateKind.LOAD);
    }

    private void validation(Object entity, EntityStateKind kind) {
        InProgressValidation inProgressValidation = new InProgressValidation(entity, kind);
        if (!inProgressValidations.contains(inProgressValidation)) {
            inProgressValidations.add(inProgressValidation);
            ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
            Validator validator = factory.unwrap(HibernateValidatorFactory.class).usingContext().constraintValidatorPayload(kind).getValidator();
            Set<ConstraintViolation<Object>> constraintViolations = validator.validate(entity);
            inProgressValidations.remove(inProgressValidation);
            if (constraintViolations != null && !constraintViolations.isEmpty()) {
                throw new ConstraintViolationException(constraintViolations);
            }

        }
    }

}
