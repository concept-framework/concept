package com.concept.data.sql.data.criteria;

import com.concept.data.core.data.criteria.ICoreCriteria;
import com.concept.data.sql.model.IBaseEntity;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

@NoRepositoryBean
public interface IBaseCriteria<TEntity extends IBaseEntity> extends ICoreCriteria<TEntity> {

}
